<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
  <title>home</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
  <link rel="stylesheet" href="assets/css/styles.min.css?h=6019a15c4819ea51d94c9b2edc773a3f">
</head>

<body style="padding: 0px;">
  <?php require 'config.php' ?>
  <div>
    <nav class="navbar navbar-light navbar-expand-md navigation-clean">
      <div class="container"><a class="navbar-brand" href="#"><img src="assets/img/EAD.png?h=596bef96c067ca4ae6d4a607faef452c" style="width: 300px;"></a><button class="navbar-toggler" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="navbar-toggler-icon"></span></button>
        <div
        class="collapse navbar-collapse" id="navcol-1">
        <ul class="nav navbar-nav ml-auto">
          <li class="nav-item text-right" role="presentation" style="width: 66px;height: 66px;"><a href="cart.php"><img href='cart.php' src="assets/img/shopping-cart.png?h=da7f20ecf3fc77633ea3a49cad403b6f" style="width: 30px;"></a></li>
          <li class="dropdown nav-item"><a class="dropdown-toggle nav-link" data-toggle="dropdown" aria-expanded="false" href="#" style="padding: 0px;font-size: 24px;"><?php if (isset($_SESSION['username'])) {
            echo $_SESSION['username'];
          }else{
            header('Location:index.php');
          }?></a>
          <div class="dropdown-menu" role="menu"><a class="dropdown-item" role="presentation" href="editprofile.php">Edit Profile</a><a class="dropdown-item" role="presentation" href="logout.php">Log Out</a></div>
        </li>
      </ul>
    </div>
  </div>
</nav>
</div>
<div>
  <?php require 'errors.php' ?>
  <div class="jumbotron">
    <h1 class="text-center text-sm-center text-md-center text-lg-center text-xl-center">Hello Coders</h1>
    <p class="text-center text-sm-center text-md-center text-lg-center text-xl-center justify-content-center align-items-center align-content-center">Welcome to our store, please take a look for the products you might buy.</p>
    <p></p>
  </div>
</div>
<div class="container text-center text-sm-center text-md-center text-lg-center text-xl-center border-primary shadow-sm d-xl-flex justify-content-center align-items-center align-content-center align-self-center m-auto justify-content-xl-center align-items-xl-center"
style="height: 600;margin: 0px;margin-left: 0px;padding: 50px;width: 1600px;margin-right: 0px;">
<?php include "buy.php" ?>
<div class="row">
  <div class="col"><img src="assets/img/html.png?h=cdd1c357f74d502405fd2078131491c5" style="width: 150px;height: 150px;">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Learning Basic Web Programming</h4>
        <h6 class="text-muted card-subtitle mb-2">Rp 210.000,-</h6>
        <p class="card-text">Want to be able to make website? Learn basic components such as HTML, CSS and JavaScript in the class curriculum. So you will get more</p>
      </div>
    </div><a class="btn btn-primary" type="button" style="width: 300px;" name="buyWeb" id="buyWeb" href='buy.php?web=true'>BUY</a></div>
    <div class="col"><img src="assets/img/java.png?h=fbf9ee4dc80354b567e6cc35c6787b27" style="width: 150px;height: 150px;">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">Starting Programming in Java</h4>
          <h6 class="text-muted card-subtitle mb-2">Rp 150.000,-</h6>
          <p class="card-text">Learn Java language for who want to learn the most popular Object-Oriented Programming concept for developing aps</p>
        </div>
      </div><a class="btn btn-primary" type="button" style="width: 300px;" name="buyJava" id="buyJava" href='buy.php?java=true'>BUY</a></div>
      <div class="col"><img src="assets/img/python.png?h=0a1b87a03abffb3f964dc18d3087907e" style="width: 150px;height: 150px;">
        <div class="card">
          <div class="card-body">
            <h4 class="card-title">Starting Programming in Python</h4>
            <h6 class="text-muted card-subtitle mb-2">Rp 200.000,-</h6>
            <p class="card-text">Learn Python fundamental various current industry trends: Data Science, Machine Learning, Infrastructure Management</p>
          </div>
        </div><a class="btn btn-primary" type="button" style="width: 300px;" name="buyPython" id="buyPython" href='buy.php?python=true'>BUY</a></div>
      </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.bundle.min.js"></script>
    <script src="assets/js/script.min.js?h=6d3c79893625a5fe5ccfeb2bca4d9446"></script>

  </body>

  </html>
