<?php
session_start();

// initializing variables
$username = "";
$email    = "";
// connect to the database
$db = mysqli_connect('localhost', 'root', '', 'MODUL 4');

// REGISTER USER
if (isset($_POST['btnFormRegister'])) {
  // receive all input values from the form
  $username = mysqli_real_escape_string($db, $_POST['username']);
  $email = mysqli_real_escape_string($db, $_POST['email']);
  $password = mysqli_real_escape_string($db, $_POST['password']);
  $password_confirm = mysqli_real_escape_string($db, $_POST['confirm_password']);

  if (empty($username)) { $_SESSION['message'] = "Username is empty!";
  $_SESSION['msg-type'] = "danger"; }
  if (empty($email)) { $_SESSION['message'] = "Email is empty!";
  $_SESSION['msg-type'] = "danger"; }
  if (empty($password)) { $_SESSION['message'] = "Password is empty!";
  $_SESSION['msg-type'] = "danger"; }
  if ($password != $password_confirm) {
    $_SESSION['message'] = "Password doesn't match!";
    $_SESSION['msg-type'] = "danger";
  }

  // first check the database to make sure
  // a user does not already exist with the same username and/or email
  $user_check_query = "SELECT * FROM users WHERE username='$username' OR email='$email' LIMIT 1";
  $result = mysqli_query($db, $user_check_query);
  $user = mysqli_fetch_assoc($result); //diubah assoc jadi array

  if ($user) { // if user exists
    if ($user['username'] === $username) {
      $_SESSION['message'] = "Username is already exist!";
      $_SESSION['msg-type'] = "danger";
    }

    if ($user['email'] === $email) {
      $_SESSION['message'] = "Email is already exist!";
      $_SESSION['msg-type'] = "danger";
    }
  }

  // Finally, register user if there are no errors in the form
  if (count($errors) == 0) {
  	$password = md5($password);//encrypt the password before saving in the database

  	$query = "INSERT INTO users (username, email, password)
  			  VALUES('$username', '$email', '$password')";
  	mysqli_query($db, $query);
  	$_SESSION['username'] = $username;
  	$_SESSION['success'] = "You are now logged in";
  	header('location: index.php');
  }
}

// LOGIN USER
if (isset($_POST['btnFormLogin'])) {
  $email = mysqli_real_escape_string($db, $_POST['email']);
  $password_input = mysqli_real_escape_string($db, $_POST['password']);

  if (empty($email)) {
    $_SESSION['message'] = "Email is empty!";
    $_SESSION['msg-type'] = "danger";
  }
  if (empty($password)) {
    $_SESSION['message'] = "Password is empty!";
    $_SESSION['msg-type'] = "danger";
  }

  if (count($errors) == 0) {
  	$password = md5($password_input);
  	$query = "SELECT * FROM users WHERE email='$email' AND password='$password'";
  	$results = mysqli_query($db, $query);
  	if (mysqli_num_rows($results) == 1) { //diubah = jadi !=
      $row = $results-> fetch_assoc();
      $_SESSION['username'] = $row['username'];
      $_SESSION['userid'] = $row['id'];
  	  $_SESSION['email'] = $email;
  	  $_SESSION['success'] = "You are now logged in";
  	  header('location: home.php');
  	}else {
      $_SESSION['message'] = "Email and password doesn't match!";
      $_SESSION['msg-type'] = "danger";
  	}
  }
  if(isset($_GET['delete'])){
    $id = $_GET['delete'];
    $db->query("DELETE FROM cart WHERE id='$id'") or die($mysqli->error()); // id diubah jadi ld
    $_SESSION['message'] = "Record has been deleted!";
    $_SESSION['msg-type'] = "danger";

    header("location: home.php");
  }

}

?>
