
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>home</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
    <link rel="stylesheet" href="assets/css/styles.min.css?h=6019a15c4819ea51d94c9b2edc773a3f">
</head>
<body style="padding: 0px;">
  <?php require 'config.php' ?>
    <div>
        <nav class="navbar navbar-light navbar-expand-md navigation-clean">
          <div class="container"><a class="navbar-brand" href="home.php"><img src="assets/img/EAD.png?h=596bef96c067ca4ae6d4a607faef452c" style="width: 300px;"></a><button class="navbar-toggler" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="navbar-toggler-icon"></span></button>
              <div
                  class="collapse navbar-collapse" id="navcol-1">
                  <ul class="nav navbar-nav ml-auto">
                      <li class="nav-item text-right" role="presentation" style="width: 66px;height: 66px;"><a href="cart.php"><img href='cart.php' src="assets/img/shopping-cart.png?h=da7f20ecf3fc77633ea3a49cad403b6f" style="width: 30px;"></a></li>
                      <li class="dropdown nav-item"><a class="dropdown-toggle nav-link" data-toggle="dropdown" aria-expanded="false" href="#" style="padding: 0px;font-size: 24px;"><?php if (isset($_SESSION['username'])) {
                        echo $_SESSION['username'];
                      }else{
                        header('Location:index.php');
                      }?></a>
                          <div class="dropdown-menu" role="menu"><a class="dropdown-item" role="presentation" href="editprofile.php">Edit Profile</a><a class="dropdown-item" role="presentation" href="logout.php">Log Out</a></div>
                      </li>
                  </ul>
          </div>
  </div>
    </nav>
    </div>
    <?php require_once 'errors.php' ?>
    <div>
        <div class="jumbotron">
            <h1 class="text-center text-sm-center text-md-center text-lg-center text-xl-center">Hello Coders</h1>
            <p class="text-center text-sm-center text-md-center text-lg-center text-xl-center justify-content-center align-items-center align-content-center">Welcome to our store, please take a look for the products you might buy.</p>
            <p></p>
        </div>
    </div>
    <div class="container text-center text-sm-center text-md-center text-lg-center text-xl-center border-primary shadow-sm d-xl-flex justify-content-center align-items-center align-content-center align-self-center m-auto justify-content-xl-center align-items-xl-center"
        style="height: 600;margin: 0px;margin-left: 0px;padding: 50px;width: 1600px;margin-right: 0px;">
        <?php
        $mysqli = mysqli_connect('localhost', 'root', '', 'MODUL 4');
        $userparam = $_SESSION['userid'];
        $result = $mysqli->query("SELECT * FROM cart WHERE user_id = '$userparam'") or die($mysqli->error);
        ?>
        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Product</th>
                        <th>Price</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                  <?php
                  while ($row = $result->fetch_assoc()){
                    ?>
                    <tr>
                      <td><?php echo $row['id']; ?></td>
                      <td><?php echo $row['product']; ?></td>
                      <td><?php echo $row['price']; ?></td>
                      <td>
                          <a href="delete.php?delete=<?php echo $row['id'] ?>" class="btn btn-danger">Delete</a>
                      </td>
                    </tr>
                  <?php } ?>
                  <?php $querysum = "SELECT SUM(price) FROM cart where user_id = '$userparam'";
                        $resultsum = mysqli_query($mysqli, $querysum);
                        $sum = $resultsum->fetch_assoc();
                   ?>
                   <tr>
                     <td colspan="3">TOTAL</td>
                     <td><?php echo implode($sum); ?></td>
                   </tr>
                </tbody>
            </table>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.bundle.min.js"></script>
    <script src="assets/js/script.min.js?h=6d3c79893625a5fe5ccfeb2bca4d9446"></script>
</body>

</html>
