<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>home</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
    <link rel="stylesheet" href="assets/css/styles.min.css?h=6019a15c4819ea51d94c9b2edc773a3f">
</head>

<body style="padding: 0px;">
  <?php require 'config.php' ?>
    <div>
      <nav class="navbar navbar-light navbar-expand-md navigation-clean">
        <div class="container"><a class="navbar-brand" href="home.php"><img src="assets/img/EAD.png?h=596bef96c067ca4ae6d4a607faef452c" style="width: 300px;"></a><button class="navbar-toggler" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="navbar-toggler-icon"></span></button>
          <div
          class="collapse navbar-collapse" id="navcol-1">
          <ul class="nav navbar-nav ml-auto">
            <li class="nav-item text-right" role="presentation" style="width: 66px;height: 66px;"><a href="cart.php"><img href='cart.php' src="assets/img/shopping-cart.png?h=da7f20ecf3fc77633ea3a49cad403b6f" style="width: 30px;"></a></li>
            <li class="dropdown nav-item"><a class="dropdown-toggle nav-link" data-toggle="dropdown" aria-expanded="false" href="#" style="padding: 0px;font-size: 24px;"><?php if (isset($_SESSION['username'])) {
              echo $_SESSION['username'];
            }else{
              header('Location:index.php');
            }?></a>
            <div class="dropdown-menu" role="menu"><a class="dropdown-item" role="presentation" href="editprofile.php">Edit Profile</a><a class="dropdown-item" role="presentation" href="logout.php">Log Out</a></div>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <?php require_once 'errors.php' ?>
    <h1 class="text-center">PROFILE</h1>
    </div>
    <div></div>
    <div class="container text-center text-sm-center text-md-center text-lg-center text-xl-center border-primary shadow-sm d-xl-flex justify-content-center align-items-center align-content-center align-self-center m-auto justify-content-xl-center align-items-xl-center"
        style="height: 600;margin: 0px;margin-left: 0px;padding: 50px;width: 1600px;margin-right: 0px;">
        <?php
        $user_id = $_SESSION['userid'];
        $db = mysqli_connect('localhost', 'root', '', 'MODUL 4');
        $query = "SELECT * FROM users WHERE id = $user_id";
        $result = mysqli_query($db, $query);
        $row = $result->fetch_assoc();
         ?>
        <form action="update.php" method="post">
            <div class="table-responsive" style="width: 900px;">
                <table class="table">
                    <thead>
                        <tr></tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Email</td>
                            <td><input class="form-control" type="text" value="<?php echo $row['email'] ?>" name="email"></td>
                        </tr>
                        <tr>
                            <td>Username</td>
                            <td><input class="form-control" type="text" value="<?php echo $row['username']; ?>" name="username" placeholder="username"></td>
                        </tr>
                        <tr>
                            <td>Mobile Number</td>
                            <td><input class="form-control" type="number" value="" name="mobilenumber" placeholder="Mobile Number"></td>
                        </tr>
                        <tr>
                            <td>New Password</td>
                            <td><input class="form-control" type="password" value="" name="newpassword" placeholder="New Password"></td>
                        </tr>
                        <tr>
                            <td style="width: 300px;">Confirm Password</td>
                            <td><input class="form-control" type="password" name="newpasswordconfirm" value="" placeholder="Confirm Password"></td>
                        </tr>
                        <tr colspan="2">
                            <td colspan="2"><input class="btn btn-primary" name="btnSaveUpdate" id="btnSaveUpdate" style="width: 600px;" type="submit"></input></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </form>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.bundle.min.js"></script>
    <script src="assets/js/script.min.js?h=6d3c79893625a5fe5ccfeb2bca4d9446"></script>
</body>

</html>
