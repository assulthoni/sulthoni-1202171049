<?php include 'config.php'; ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
  <title>home</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
  <link rel="stylesheet" href="assets/css/styles.min.css?h=7f2eef728c264a8035fd9e575b4bc855">
</head>

<body style="padding: 0px;">
  <div>
    <nav class="navbar navbar-light navbar-expand-md navigation-clean-button" style="padding: 0px;">
      <div class="container"><a class="navbar-brand" href="index.html"><img src="assets/img/EAD.png?h=596bef96c067ca4ae6d4a607faef452c" style="width: 300px;"></a><button class="navbar-toggler" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="navbar-toggler-icon"></span></button>
        <div
        class="collapse navbar-collapse text-right text-sm-right text-md-right text-lg-right text-xl-right float-right d-xl-flex mr-auto justify-content-xl-end" id="navcol-1" style="width: 1000px;"><span class="navbar-text actions"> <a href="#" id="btnLogin" class="login">Log In</a><a class="btn btn-light action-button" role="button" href="#" data-bs-hover-animate="swing" id="btnSignup">Sign Up</a></span></div>
      </div>
    </nav>
    <?php require_once 'errors.php' ?>
    <div class="jumbotron">
      <h1 class="text-center text-sm-center text-md-center text-lg-center text-xl-center">Hello Coders</h1>
      <p class="text-center text-sm-center text-md-center text-lg-center text-xl-center justify-content-center align-items-center align-content-center">Welcome to our store, please take a look for the products you might buy.</p>
      <p></p>
    </div>
  </div>
  <div class="container text-center text-sm-center text-md-center text-lg-center text-xl-center border-primary shadow-sm d-xl-flex justify-content-center align-items-center align-content-center align-self-center m-auto justify-content-xl-center align-items-xl-center"
  style="height: 600;margin: 0px;margin-left: 0px;padding: 50px;width: 1600px;margin-right: 0px;">
  <div class="row">
    <div class="col"><img src="assets/img/html.png?h=cdd1c357f74d502405fd2078131491c5" style="width: 150px;height: 150px;">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">Learning Basic Web Programming</h4>
          <h6 class="text-muted card-subtitle mb-2">Rp 210.000,-</h6>
          <p class="card-text">Want to be able to make website? Learn basic components such as HTML, CSS and JavaScript in the class curriculum. So you will get more</p>
        </div>
      </div><button class="btn btn-primary" type="button" style="width: 300px;">BUY</button>
    </div>
    <div class="col"><img src="assets/img/java.png?h=fbf9ee4dc80354b567e6cc35c6787b27" style="width: 150px;height: 150px;">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">Starting Programming in Java</h4>
          <h6 class="text-muted card-subtitle mb-2">Rp 150.000,-</h6>
          <p class="card-text">Learn Java language for who want to learn the most popular Object-Oriented Programming concept for developing aps</p>
        </div>
      </div><button class="btn btn-primary" type="button" style="width: 300px;">BUY</button>
    </div>
    <div class="col"><img src="assets/img/python.png?h=0a1b87a03abffb3f964dc18d3087907e" style="width: 150px;height: 150px;">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">Starting Programming in Python</h4>
          <h6 class="text-muted card-subtitle mb-2">Rp 200.000,-</h6>
          <p class="card-text">Learn Python fundamental various current industry trends: Data Science, Machine Learning, Infrastructure Management</p>
        </div>
      </div><button class="btn btn-primary" type="button" style="width: 300px;">BUY</button>
    </div>
  </div>
</div>
<div class="modal fade" role="dialog" tabindex="-1" id="myModalLogin">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">LOGIN</h4><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button></div>
        <div class="modal-body">

          <form action="index.php" method="post" name="formLogin">
            <div class="form-group">
              <p>Email</p><input class="form-control" type="email" placeholder="Type Your Email" name="email">
            </div>
            <div class="form-group">
              <p>Password</p><input class="form-control" type="password" placeholder="Type Your Password" name="password">
            </div>
            <div class="form-group">
              <input class="btn btn-primary" type="submit" name="btnFormLogin" id="btnFormLogin" value="Login"></input>
            </div>
          </form>
        </div>

      </div>
    </div>
  </div>
  <div class="modal fade" role="dialog" tabindex="-1" id="myModalRegister">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Register</h4><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button></div>
          <div class="modal-body">
            <form name="formRegister" action="index.php" method="post">
              <div class="form-group">
                <p>Email</p><input class="form-control" type="email" placeholder="Type Your Email" name="email" value="<?php echo $email ?>">
              </div>
              <div class="form-group">
                <p>Username</p><input class="form-control" type="text" placeholder="Type your username" name="username" value="<?php echo $username ?>">
              </div>
              <div class="form-group">
                <p>Password</p><input class="form-control" type="password" placeholder="Type your password" name="password">
              </div>
              <div class="form-group">
                <p>Retype Password</p><input class="form-control" type="password" placeholder="Retype your password" name="confirm_password">
              </div>
              <div class="form-group">
                <input class="btn btn-primary" type="submit" name="btnFormRegister" id="btnFormRegister" value="Register"></input>
              </div>
            </form>
          </div>

        </div>
      </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.bundle.min.js"></script>
    <script src="assets/js/script.min.js?h=6d3c79893625a5fe5ccfeb2bca4d9446"></script>
    <script type="text/javascript">
      $(document).ready(function(){
        $('#btnFormLogin').click(function(){
          var email = $('email').val();
          var password = $('password').val();

          if(email ='' || password = ''){
            alert('Both field are required');
          }
        });
      });
    </script>
  </body>

  </html>
