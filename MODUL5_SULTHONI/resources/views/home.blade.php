@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>
                <div class="card-body">
                      @if(isset($posts))
                        @foreach($posts as $post)
                          <div class="card">
                            <div class="card-header">
                              <img src="{{asset(''.$post->user->avatar)}}" alt="avatar" class="avatar">
                              <span class="post-user">{{ $post->user->name}}</span>
                            </div>
                            <div class="card-body">
                              @if(session('status'))
                              <div class="alert alert-success" role="alert">
                                  {{ session('status') }}
                              </div>
                              @endif
                              <img src="{{asset(''.$post->image)}}" alt="{{asset(''.$post->image)}}" class="post-image">
                            </div>
                            <div class="card-footer">
                              <div class="col-sm-12 text-left">
                                <span class="post-user">{{$post->user->email}}</span>
                              </div>
                              <div class="col-sm-12 text-left">
                                <span class="post-caption">{{$post->caption}}</span>
                              </div>
                            </div>
                          </div>
                      @endforeach
                    @endif

                    <!-- You are logged in! -->
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
