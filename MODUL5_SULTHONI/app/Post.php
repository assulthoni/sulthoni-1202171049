<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    //
    protected $table = "posts";

    // $user = DB::table('users')->get();
    // $user = DB::table($table)->where('user_id', auth()->id());

    public function user()
    {
      // code...
      return $this->belongsTo(User::class, 'user_id','id');
    }
}
