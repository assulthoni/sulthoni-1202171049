<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
class PostTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    private function getUserRandom()
    {
      // code...
      $user = DB::table('users')->
    }
    public function run()
    {
        //
        $userIdArr = [
          1,2,3
        ];
        $postImageArr =[
            'postImage1.png',
            'postImage2.jpg',
        ];
        DB::table('posts')->insert([
          'user_id' => implode(Arr::random($userIdArr,1)),
          'caption' => Str::random(20),
          'image' => implode(Arr::random($postImageArr,1))
        ]);
    }
}
