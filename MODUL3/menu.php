<?php
$nama = $_GET["nama-driver"];
$nohp = $_GET["hp-driver"];
$tgl = $_GET["tanggal-pesan"];
$driver = $_GET["driver"];



 ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <style media="screen">
    .left-container{
      width: 50%;
      float: left;
      text-align: center;
    }

    .right-container{
      width: 50%;
      text-align: center;
      float: right;
    }
    .right-container{
      text-align: center;
      width: 50%;
      margin-left: auto;
      margin-right: auto;
      background-color: #dddddd;
    }
    .right-container input[type=submit], input[type=reset] {
      background-color: #16A5B7;
      border: none;
      color: white;
      padding: 16px 32px;
      text-decoration: none;
      margin: 4px 2px;
      cursor: pointer;
    }
    table{
      margin-left: auto;
      margin-right: auto;
      border-collapse: collapse;
      width:50%;
      text-align: left;
      /* border-color: black; */
    }
    table tr td{
      height: 60px;
      width: 100px;
      text-align: left;
    }

    h1{
      font-family: Arial, Helvetica, sans-serif;
    }
    </style>
    <link rel="stylesheet" href="css/stylemenu.css">
    <meta charset="utf-8">
    <title>Menu</title>
  </head>
  <body>
    <div class="left-container">
      <h1>~Data Driver Ojol~</h1>
      <label><b>Nama</b></label>
      <p><?=$nama?></p>
      <label><b>No Hp</b></label>
      <p><?= $nohp?></p>
      <label><b>Tanggal</b></label>
      <p><?= $tgl?></p>
      <label><b>Driver dari</b></label>
      <p><?= $driver?></p>
      <label><b>Bawa Kantong</b></label>
      <?php if (isset($_GET["kantong-belanja"])&& $_GET["kantong-belanja"] =="Ya") {
        // code...
        echo "<p>Bawa</p>";
      }else {
        echo "<p>Tidak Bawa</p>";
      }?>
    </div>
    <div class="right-container">
      <form class="form_menu" action="nota.php" method="post">
        <h1>~Menu~</h1>
        <p>Pilih Menu</p>
        <table>
          <tr>
            <td><input type="checkbox" name="menu[]" value="28000">Es Coklat Susu</td>
            <td><p>Rp 28.000,-</p></td>
          </tr>
          <tr>
            <td><input type="checkbox" name="menu[]" value="18000">Es Susu Matcha</td>
            <td><p>Rp 18.000,-</p></td>
          </tr>
          <tr>
            <td><input type="checkbox" name="menu[]" value="15000">Es Susu Mojicha</td>
            <td><p>Rp 15.000,-</p></td>
          </tr>
          <tr>
            <td><input type="checkbox" name="menu[]" value="30000">Es Matcha Latte</td>
            <td><p>Rp 30.000,-</p></td>
          </tr>
          <tr>
            <td><input type="checkbox" name="menu[]" value="21000">Es Taro Susu</td>
            <td><p>Rp 21.000,-</p></td>
          </tr>
        </table>
        <table>
          <tr>
            <td><b>Nomor Order</b></td>
              <td>
                <input type="number" name="no-order" required>
              </td>
          </tr>
          <tr>
              <td>
                <b>Nama Pemesan</b>
              </td>
              <td><input type="text" name="nama-pemesan" required></td>
          </tr>
          <tr>
              <td>
                <b>Email</b>
              </td>
              <td><input type="email" name="email" required></td>
          </tr>
          <tr>
              <td>
                <b>Alamat Order</b>
              </td>
              <td><input type="text" name="alamat" required></td>
          </tr>
          <tr>
              <td>
                <b>Member</b>
              </td>
              <td><input type="radio" name="member" value="Ya">Ya<input type="radio" name="member" value="No">Tidak</td>
          </tr>
          <tr>
            <td><b>Metode Bayar</b></td>
            <td><select class="metode-bayar" name="mtd-bayar">
                <option value="Cash">Cash</option>
                <option value="E-Money (OVO/Gopay)">E-money (OVO/Gopay)</option>
                <option value="Credit Card">Credit Card</option>
                <option value="Lainnya">Lainnya</option>
            </select></td>
          </tr>
          <tr>
            <td colspan="2"><input style="text-align:center" type="submit" name="btnsubmit" onclick="alert" value="Cetak Struk"></td>
          </tr>
        </table>
      </form>
    </div>
  </body>
</html>
