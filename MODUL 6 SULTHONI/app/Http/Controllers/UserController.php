<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use app\User;
use Carbon\Carbon;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        //
        $get['user'] = User::withCount('post')->where('id',$id)->first();

        return view('profile',$get);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $get['user'] = Auth::user();
        return view('edit',$get);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $user = User::find($id);
        $user->title = $request->title;
        $user->description = $request->description;
        $user->url = $request->url;
        $user->updated_at = Carbon::now();

        if ($request->avatar != null) {
            $file = $request->file('avatar');
            $file_name = $file->getClientOriginalName();
            $file->move(public_path('/'), $file_name);

            $user->avatar = $file_name;
        }

        $user->save();
        return redirect('/profile/' . $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
