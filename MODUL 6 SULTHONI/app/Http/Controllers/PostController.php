<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PostComment;
use App\Post;
use Carbon\Carbon;
use Auth;
class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('addpost');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('addpost');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $file = $request->file('image');
        $namafile = $file->getClientOriginalName();
        $file->move(public_path('/'), $namafile);

        $post = Post::create(
          array('user_id'=>Auth::user()->id,
            'caption' => $request->caption,
            'image' => $namafile,
            'created_by' => Carbon::now(),
            'updated_by' => Carbon::now(),
          )
        );
        return redirect()->route('home');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $data['post'] = Post::with(['user', 'comments.user'])->where('id', $id)->first();

      return view('detailpost', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function comment(Request $request)
    {
      // code...
      $comment = PostComment::create([
        'user_id' => Auth::user()->id,
        'post_id' =>$request->post_id,
        'comment' =>$request->comment,
        'created_by' =>Carbon::now(),
        'updated_by' =>Carbon::now(),
      ]);
      return back();
    }
}
