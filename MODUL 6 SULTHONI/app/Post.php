<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    //
    protected $table = "posts";
    protected $fillable = ['caption','image', 'user_id'];

    // $user = DB::table('users')->get();
    // $user = DB::table($table)->where('user_id', auth()->id());

    public function user()
    {
      // code...
      return $this->belongsTo('App\User', 'user_id','id');
    }
    public function comments()
    {
      // code...
      return $this->hasMany('App\PostComment', 'post_id', 'id');
    }
}
