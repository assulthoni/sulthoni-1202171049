<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostComment extends Model
{
    //
    protected $table = 'post_comment';

    protected $fillable = ['user_id','post_id','comment'];

    public function user()
    {
      // code...
      return $this->belongsTo('App\User','user_id','id');
    }
    public function post()
    {
      // code...
      return $this->belongsTo('App\Post','post_id','id');
    }
}
