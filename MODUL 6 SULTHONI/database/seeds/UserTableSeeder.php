<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Arr;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $titleArr = [
          'Mr.',
          'Mrs.',
          'Ms.'
        ];
        DB::table('users')->insert([
          'name' => Str::random(10),
          'email' => Str::random(10).'@instaead.com',
          'password' => bcrypt('password'),
          'title' => implode(Arr::random($titleArr,1)),
          'description' => Str::random(20),
          'url' => 'urlUser',
          'avatar' => 'avatarUser.jpg'
        ]);
    }
}
