<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['middleware' => ['auth']], function () {
  Route::get('/profile/{id}', 'UserController@index')->name('profile');
  Route::get('/home', 'HomeController@index')->name('home');
  Route::get('/profile/edit/{id}','UserController@edit')->name('edit');
  Route::get('/post','PostController@create')->name('post');
  Route::post('/post', 'PostController@store')->name('post');
  Route::post('/comment', 'PostController@comment')->name('comment');
  Route::put('/profile/{id}', 'UserController@update');
  Route::get('/detail/{id}', 'PostController@show')->name('detailpost');
});
