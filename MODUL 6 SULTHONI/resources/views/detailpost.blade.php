@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <img src="{{ asset('' . $post->image) }}" class="post-image-detail">
            </div>
            <div class="col-sm-6">
                <div class="row col-sm-12">
                    <a href="{{ url('profile/' . $post->user->id) }}">
                        <img src="{{ asset('' . $post->user->avatar) }}" class="avatar">
                    </a>
                    <span class="post-user flex">{{ $post->user->name }}</span>
                </div>
                <hr>
                <div class="row col-sm-12 text-left">
                    <span class="post-user">{{ $post->user->email }}</span>
                    <span class="post-caption">{{ $post->caption }}</span>
                </div>
                @foreach($post->comments as $comment)
                    <div class="row col-sm-12 text-left">
                        <span class="post-user">{{ $comment->user->email }}</span>
                        <span class="post-caption">{{ $comment->comment }}</span>
                    </div>
                @endforeach
                <hr>
                <div class="row col-sm-12 text-left">
                <img  src="/assets/like.png" style="padding: 10px 0"></img>
                <img  src="/assets/comment.png" style="padding: 10px 15px"></img>
                </div>
                <div class="row col-sm-12 text-left">
                    <span class="post-user">{{ $post->likes }} Likes</span>
                </div>
                <form action="{{ url('comment') }}" method="POST">
                    <div class="row col-sm-12">
                        @csrf
                        <input type="hidden" name="post_id" value="{{ $post->id }}">
                        <div class="input-group mt-2">
                            <input type="text" class="form-control" name="comment" placeholder="Add a comment...">
                            <div class="input-group-append">
                                <button class="btn btn-outline-secondary" type="submit">Post</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
