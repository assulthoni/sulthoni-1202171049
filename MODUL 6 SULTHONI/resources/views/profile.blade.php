@extends('layouts.app')

@section('content')
<div class="container">
        <div class="row mb-lg-5">
            <div class="col-sm-4 text-center pt-lg-5">
                <img src="{{ asset('' . $user->avatar) }}" class="avatar-profile" alt="avatar">
            </div>
            <div class="col-sm-8">
                <div class="row mt-4 mb-4">
                    <div class="col-sm-8 post-user" style="padding: 0">
                        {{ $user->name }}
                    </div>
                    <div class="col-sm-4 text-right">
                        <a href="{{ route('post') }}" class="text-right">Add New Post</a>
                    </div>
                </div>
                <div class="row">
                    <a href="{{ url('profile/edit/'.$user->id) }}">Edit Profile</a>
                </div>
                <div class="row mb-4">{{ $user->post_count }} Posts</div>
                <div class="row post-user">{{ $user->title }}</div>
                <div class="row">{{ $user->description }}</div>
                <div class="row">
                    <a href="{{ $user->url }}">{{ $user->url }}</a>
                </div>
            </div>
        </div>
        @foreach($user->post as $post)
            @if(($loop->iteration % 3 - 1) == 0)
                <div class="row">
                    @endif
                    <div class="col-sm-4">
                        <a href="{{ url('detail/' . $post->id) }}">
                            <img src="{{ asset('' . $post->image) }}" class="post-image-profile"
                             alt="{{ asset('' . $post->image) }}">
                        </a>
                    </div>
                    @if($loop->iteration % 3 == 0)
                </div>
            @endif
        @endforeach
    </div>
    </div>
@endsection
