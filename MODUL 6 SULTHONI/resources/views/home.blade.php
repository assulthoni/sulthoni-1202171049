@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>
                <div class="card-body">
                      @if(isset($posts))
                        @foreach($posts as $post)
                          <div class="card">
                            <div class="card-header">
                              <img src="{{asset(''.$post->user->avatar)}}" alt="avatar" class="avatar">
                              <span class="post-user">{{ $post->user->name}}</span>
                            </div>
                            <div class="card-body">
                              @if(session('status'))
                              <div class="alert alert-success" role="alert">
                                  {{ session('status') }}
                              </div>
                              @endif
                              <a href="{{url('detail/'.$post->id)}}"><img src="{{asset(''.$post->image)}}" alt="{{asset(''.$post->image)}}" class="post-image"></a>
                            </div>
                            <div class="card-footer">
                              <div class="col-sm-12 text-left">
                                <a href=""><img src="assets/like.png" alt="like"></a>
                                <a href=""><img src="assets/comment.png" alt="comment"></a>
                              </div>
                              <div class="col-sm-12 text-left">
                                <span class="post-user">{{$post->user->email}}</span>
                              </div>
                              <div class="col-sm-12 text-left">
                                <span class="post-caption">{{$post->caption}}</span>
                              </div>
                              @isset($post->comments)
                              @foreach($post->comments as $comment)
                                  <div class="col-sm-12 text-left">
                                      <span class="post-user">{{ $comment->user->email }}</span>
                                      <span class="post-caption">{{ $comment->comment }}</span>
                                  </div>
                              @endforeach
                              @endisset
                              <div class="col-sm-12">
                                    <form action="{{ url('comment') }}" method="POST">
                                        @csrf
                                        <input type="hidden" name="post_id" value="{{ $post->id }}">
                                        <div class="input-group mt-2">
                                            <input type="text" class="form-control" name="comment"
                                                   placeholder="Add a comment...">
                                            <div class="input-group-append">
                                                <button class="btn btn-outline-secondary" type="submit">Post</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                          </div>
                      @endforeach
                    @endif

                    <!-- You are logged in! -->
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
